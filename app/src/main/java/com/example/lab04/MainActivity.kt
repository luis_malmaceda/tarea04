package com.example.lab04

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ArrayAdapter
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import com.example.lab04.Model.Solicitud
import com.example.lab04.databinding.ActivityMainBinding
import com.example.lab04.databinding.VistaPreviaBinding

class MainActivity : AppCompatActivity() {
    private lateinit var binding:ActivityMainBinding
    var listMarcas : MutableList<String> = mutableListOf()
    private lateinit var registro:Solicitud
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        Init()


        binding.btnConfirmar.setOnClickListener {
            if(binding.edtNombre.text.toString().isEmpty()){
                Toast.makeText(this,"Ingresa nombre del titular",Toast.LENGTH_SHORT).show()
                binding.edtNombre.requestFocus()
                return@setOnClickListener
            }
            if(binding.spMarcas.selectedItemPosition ==0){
                Toast.makeText(this,"Selecciona Marca",Toast.LENGTH_SHORT).show()
                binding.spMarcas.requestFocus()
                return@setOnClickListener
            }
            if(binding.edtAnio.text.toString().isEmpty()){
                Toast.makeText(this,"Ingresa año del vehículo",Toast.LENGTH_SHORT).show()
                binding.edtAnio.requestFocus()
                return@setOnClickListener
            }
            if(binding.edtDescProblema.text.toString().isEmpty()){
                Toast.makeText(this,"Ingresa una descripción detallada del problema",Toast.LENGTH_SHORT).show()
                binding.edtDescProblema.requestFocus()
                return@setOnClickListener
            }
            val nombreTitular = binding.edtNombre.text.toString()
            val marca = binding.spMarcas.selectedItem.toString()
            val anio = binding.edtAnio.text.toString().toInt()
            val desProblema = binding.edtDescProblema.text.toString()

            registro = Solicitud(nombreTitular,marca,anio,desProblema)

            AbrevistaPrevia().show()



        }
    }

    private fun AbrevistaPrevia():AlertDialog {
        val alertdialog:AlertDialog
        val view = layoutInflater.inflate(R.layout.vista_previa,null)
        val builder = AlertDialog.Builder(this)
        builder.setView(view)

        val tvNombreTitular: TextView = view.findViewById(R.id.tvNomTitular)
        val tvMarca: TextView = view.findViewById(R.id.tvMarca)
        val tvAnio: TextView = view.findViewById(R.id.tvAnio)
        val tvProblema: TextView = view.findViewById(R.id.tvDescripcionProblema)
        val btnCerrar : Button = view.findViewById(R.id.btnCerrar)

        tvNombreTitular.text = registro.nombreTitular
        tvMarca.text = registro.marca
        tvAnio.text = registro.anio.toString()
        tvProblema.text = registro.descProblema
        alertdialog = builder.create()

        btnCerrar.setOnClickListener {
            alertdialog.dismiss()
        }

        return  alertdialog
    }

    private fun Init():Unit {
        listMarcas.add("Selecciona Marca")
        listMarcas.add("BMW")
        listMarcas.add("Mercedes-Benz")
        listMarcas.add("Audi")
        listMarcas.add("Lexus")
        listMarcas.add("Renault")
        listMarcas.add("Ford")
        listMarcas.add("Opel")
        listMarcas.add("Seat")

        val adaptador =  ArrayAdapter(this,R.layout.item_marca,listMarcas)
        binding.spMarcas.adapter = adaptador
    }


}